# Test Webserver

### Install 

#### Create virtual environment

```bash
> virtualenv --python /usr/bin/python3.6 python3.6
> source python3.6/bin/activate
```

#### Install required packages

```bash
> pip3 install -r requirements.txt
```

### Run

```bash
> python app.py
```

With `http:\\localhost:5050` you get the following output in the browser:

You have reached the Vulnerable App.

Here are the list of urls you would need while using this application

 API | Function | Allowed methods | Description
 ----|----------|-----------------|-------------
 `/`  |  `sitemap` |  `HEAD,OPTIONS,GET` | Create overview of all interfaces
 `/login` | `login` |  `POST,OPTIONS` | User login, return value is an `access_token` needed to access the interfaces `/fetch/customer`, `/get/<customerID>` and `/search`
`/get/<customerID>` | `get_customer` | `GET` | Get customer data from customer with ID <customerID>
 `/search` | `search_customer` |  `POST,OPTIONS` | Search for customer with username `"search": "username"`
 `/fetch/customer` | `fetch_customer` | `POST,OPTIONS` | Fetch customer with ID `"id": "ID"`
 `/register/user` | `reg_customer` |  `POST,OPTIONS` | Register user with `"username": "Name"` and `"password": "passwd"`
 `/register/customer` | `reg_user` | `POST,OPTIONS` | Register customer with `"username": "username"`, `"password": "password`, `"first_name": "first_name"`, `"last_name": "last_name`, `"email": "email"` and `"ccn": "ccn"`
 `/xxe_uploader` | `hello` |  `POST,HEAD,OPTIONS,GET` | internal service (see `/xxe`)
 `/yaml_hammer` | `yaml_hammer` | `POST,OPTIONS` | internal service (see `/yaml_hammer`)
 `/yaml` | `yaml_upload` |  `HEAD,OPTIONS,GET` | Web UI to select a yml file and display it on a new web page converting it to json format beforehand (with `/yaml_hammer`)
 `/xxe` | `index` |  `HEAD,OPTIONS,GET` | Web UI to select a docx file to convert it to text and display it on another web page (with `/xxe_uploader`)

### Test

To execute the functional tests proceed as follows

```bash
> cd tests
> ./run_tests.sh
```

### Kubernetes setup

[Configuring GitLab with group-level Kubernetes cluster](https://about.gitlab.com/handbook/customer-success/demo-systems/tutorials/getting-started/configuring-group-cluster/)
