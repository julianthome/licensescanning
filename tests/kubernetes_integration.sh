# steps to execute for manual setup of kubernetes cluster

# 1. authenticate
gcloud auth login --no-launch-browser

gcloud auth list


gcloud iam service-accounts keys create
gcloud auth activate-service-account test-service-account@google.com --key-file=/path/key.json --project=testproject

# 1. configure
export PROJECT_ID=moonlit-shadow-326813
export USER_EMAIL=mark.busenhart@roche.com
export IMAGE=webapp
export ZONE=europe-west3-a
export REGION=europe-west3


export APP_NAME=webapp
export CLUSTER_NAME=${APP_NAME}-cluster


gcloud config set account ${USER_EMAIL}
gcloud config set compute/zone ${ZONE} 
gcloud config set compute/region ${REGION}


# 2. authenticate
gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}
gcloud container clusters get-credentials webapp-cluster --zone europe-west3-a --project moonlit-shadow-326813



kubectl create ns gitlab-managed-apps

# Download Helm chart values that is compatible with the requirements above.
# These are included in the Cluster Management project template.
wget https://gitlab.com/gitlab-org/project-templates/cluster-management/-/raw/master/applications/prometheus/values.yaml

# Add the Prometheus community Helm chart repository
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts

# Install Prometheus
helm install prometheus prometheus-community/prometheus -n gitlab-managed-apps --values values.yaml