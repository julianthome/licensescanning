
docker builder build -t webapp .



docker container run -d  -p 5000:5000 --name webapp-cont webapp

docker container run -t --name newman-cont -v $(pwd)/tests:/etc/newman/ dannydainton/htmlextra run test-webserver.postman_collection.json -e test-webserver.postman_environment.json -r htmlextra --reporter-htmlextra-export /etc/newman/results/.

docker network create nat
docker network create --driver=bridge --subnet=127.0.0.0/24 test_nat


docker container run -d  -p 5050:5050 --name webapp-cont --network nat webapp
docker container run -t --name newman-cont --network nat -v $(pwd)/tests:/etc/newman/ dannydainton/htmlextra run test-webserver.postman_collection.json -e test-webserver.postman_environment.json -r htmlextra --reporter-htmlextra-export /etc/newman/results/.


docker cp container_id:/newman/hmltextra .

newman run test-webserver.postman_collection.json -e test-webserver.postman_environment.json -r htmlextra --reporter-htmlextra-export /test/tests/.

docker container run -it --name ubuntu-cont --network nat -v $(pwd)/tests:/etc/newman/ my-ubuntu-img:v1
docker container run -it --name ubuntu-cont --network test-webserver_default -v $(pwd)/tests:/etc/newman/ my-ubuntu-img

newman run test-webserver.postman_collection.json -e test-webserver.postman_environment-cont.json 

docker-compose up -d

docker builder build -t my-newman .
docker container run -it --name newman-cont -v $(pwd)/tests:/etc/newman/ --network test-webserver_default my-newman 
docker container run -it --name my-ubuntu-cont --network test-webserver_default  my-ubuntu-img


# authenticate
gcloud auth login
gcloud auth list

# configure
export PROJECT_ID=moonlit-shadow-326813
export USER_EMAIL=mark.busenhart@roche.com
export IMAGE=webapp
export ZONE=europe-west3-a
export REGION=europe-west3

export APP_NAME=webapp
export CLUSTER_NAME=${APP_NAME}-cluster
export CLUSTER_NAME=cluster-1

gcloud config set compute/zone ${ZONE} 
gcloud config set compute/region ${REGION}

# enable API's
gcloud services enable \
 --project=${PROJECT_ID} \
 container.googleapis.com \
 gkeconnect.googleapis.com \
 gkehub.googleapis.com \
 cloudresourcemanager.googleapis.com \
 iam.googleapis.com


# validate API's
kubectl api-resources
kubectl api-resources --namespaced=true      # Alle Ressourcen im Namespace
kubectl api-resources --namespaced=false     # Alle nicht im Namespace befindlichen Ressourcen
kubectl api-resources -o name                # Alle Ressourcen mit einfacher Ausgabe (nur der Ressourcenname)
kubectl api-resources -o wide                # Alle Ressourcen mit erweiterter Ausgabe (aka "Wide")
kubectl api-resources --verbs=list,get       # Alle Ressourcen, die "list" und "get" Verben unterstützen anfordern
kubectl api-resources --api-group=extensions # Alle Ressourcen in der API-Gruppe "extensions"




# Grant the required IAM roles to the user registering the cluster
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
 --member user:${USER_EMAIL} \
 --role=roles/gkehub.admin \
 --role=roles/iam.serviceAccountAdmin \
 --role=roles/iam.serviceAccountKeyAdmin \
 --role=roles/resourcemanager.projectIamAdmin

# create cluster
gcloud container clusters create --machine-type=e2-micro  ${CLUSTER_NAME}

# configuration
export INGRESS_ENDPOINT=34.141.95.16  # from ...
export PORT=5050

gcloud config set proxy/type http
gcloud config set proxy/address ${INGRESS_ENDPOINT}
gcloud config set proxy/port ${PORT}

# authenticate
gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}

# deploy
kubectl create deployment web-app --image=${IMAGE}
kubectl scale deployment web-app --replicas=2
kubectl autoscale deployment web-app --cpu-percent=80 --min=1 --max=5

# expose service
kubectl expose deployment web-app --name=web-app-service --type=LoadBalancer --port 5050 --target-port 5050

# admin
kubectl get pods
kubectl get service

# shut-down
kubectl delete service web-app-service
kubectl delete deployment web-app
gcloud deployment-manager deployments delete web-app


# set up from shell
gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}
kubectl create deployment ${APP_NAME} --image=${IMAGE}
kubectl scale deployment ${APP_NAME} --replicas=2
kubectl autoscale deployment ${APP_NAME} --cpu-percent=80 --min=1 --max=5
kubectl expose deployment ${APP_NAME} --name=${APP_NAME}-service --type=LoadBalancer --port 5050 --target-port 5050

# setup from gcp ([https://about.gitlab.com/handbook/customer-success/demo-systems/tutorials/getting-started/configuring-group-cluster/)

export PROJECT_ID=moonlit-shadow-326813
export ZONE=europe-west3-a
export REGION=europe-west3
export APP_NAME=webapp
export CLUSTER_NAME=${APP_NAME}-cluster

gcloud container clusters get-credentials ${CLUSTER_NAME} --zone ${ZONE} --project ${PROJECT_ID}

kubectl autoscale deployment ${APP_NAME} --cpu-percent=80 --min=1 --max=5


kubectl cluster-info | grep 'Kubernetes control plane' | awk '/http/ {print $NF}'

kubectl create sa gitlab-admin -n kube-system
# kubectl get serviceaccounts
# kubectl get serviceaccounts/gitlab -o yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  creationTimestamp: "2021-10-11T14:35:55Z"
  name: gitlab
  namespace: default
  resourceVersion: "2214"
  uid: e24e74ed-bdce-4a1c-b69c-45043a369c86
secrets:
- name: gitlab-token-5bl87


kubectl create clusterrolebinding gitlab-admin --serviceaccount=kube-system:gitlab-admin --clusterrole=cluster-admin

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')

Endpoint:
https://35.234.98.3

Cluster certificate:
-----BEGIN CERTIFICATE-----
MIIELTCCApWgAwIBAgIRAJlheIGEiTm2hXyABzFvX0wwDQYJKoZIhvcNAQELBQAw
LzEtMCsGA1UEAxMkOTlkYTQ1N2ItODllOC00M2Y5LTkzZDAtYzhkMmZiZWNhNzFj
MCAXDTIxMTAxMTE1MzA0NVoYDzIwNTExMDA0MTYzMDQ1WjAvMS0wKwYDVQQDEyQ5
OWRhNDU3Yi04OWU4LTQzZjktOTNkMC1jOGQyZmJlY2E3MWMwggGiMA0GCSqGSIb3
DQEBAQUAA4IBjwAwggGKAoIBgQDMf/PArMuvuWh62yEvAXtNT5sIsOSKlrtG8BZr
YAzgSFnVFIGHD1njXOzUrkQcZZyht4vJry2gH2owCHAmXivlKyTJMkrh+0PjsfJJ
GOoxza1im7980fu3HrqDinaiWQlJzvOxvJqoCRNrWglWC10QZxbQkulRUkWrjecd
olYJrGoMZv1DhbLUsCtDAHSjJTqlFKFSMXot8c+3twdfbtqOb/q8eQUI31rEzFmB
NC6PNGHOtn+aR8nAZWMeM0C5ktHj/g0YwEyK43KzgV4arsXbYyWC7069YWI0D/vE
U5RbFoy/AOAl1LKJX/SI0L2WKyKyueKFc1Jux4ISuf9Udt+mMwlnzCwjDVsz8Lbl
+phT7wFY/tplcTK1GbV9t3m7a6r/VQNwx8cSAh/LN+Zl8PVZfa9NE4WPBFL4N2hQ
TrbJj+ePsi7OXhJmyITMGYABdaVz92KYal8hzywI2eioC6tCgHPpAq8dOvz4/clR
O03cYR0y6+lIfW8hKS40lAj3PMsCAwEAAaNCMEAwDgYDVR0PAQH/BAQDAgIEMA8G
A1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFLjkTA+QWEAgvXYRwHN00J2C2p6eMA0G
CSqGSIb3DQEBCwUAA4IBgQCDHhZAa3MCbuB/CL6CG+3dlFkP4cQ/ILKAh5IoQy2C
2kKwhEkT1vKL3EvRpD9rWpzjCRwgGn6uzJxQdE04GDrS7ftjH4lomcA0gTs6eRfk
la/jZLRPLt5zdBwLwqNx2oB7wF2jJXA8QaB0ORNsD8AWjbAQ0pwbm55YlcQAWy0u
n4VO1vp/8zL2htufwA/+Nap2lKDa+f2JhVO/FMVFPgpOQShgyEfs3VJw6s2PdXfQ
IBIurCs5NQeoHNqviBeRt0zj5b5fINPiMvZSpfserpiuQohGo/fs53nUk52uvO/I
cNFrSU9mVgX6jZj6NzIyXxja85B5xjjIOoevp3P+Ve7qI7fitmgBienAbweq/YzI
XZlvbxpaYFnUdrb1+jgpf2tcymPtlEudijPEGPx9UXXvfmTMcdkXaeEDA2R4wbaS
XCg+UOmeuL3s7zEc7WH3lhCG8UgA2Afg7xdmvw7PW/mtnpyel/JDAxP6GaiwHn5w
jzxxRinqQpDzepTpC3bt98A=
-----END CERTIFICATE-----

Token:
eyJhbGciOiJSUzI1NiIsImtpZCI6ImVOQjh5VHZfUlRVT1J6QjU4QUhkN1BHZ0swelFsT0pCX2dWVTF5em00aWMifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJnaXRsYWItYWRtaW4tdG9rZW4teDVqMjYiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZ2l0bGFiLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiODU2YTE1ZGEtMDE5MS00MDZjLTg4YmItNzdiODU4OGYzOTQyIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmdpdGxhYi1hZG1pbiJ9.WF-iZ2sTqqbgL-rtAkRX0u6lsV2ohBrW7GEonmdu7ztufO9DCRUr6xyvVqoL4QnmIzySYgUk39339ZYlREfPxBT2M-P0vz5AdesO8K_Dt_RGWST99wZb7-xF_hGaZ_Zts8VU1FClMP4TFMx292PakCscyAtVLXX6VBaYJBn-e4U3ulgerxYJqGQZ9rT9YEgEUMF-1aliMESyup6eHB-H0drdHLq4XEExd34lnZyBXoDj9g23oyNMxOnv7lCq-zkL7Vd22XOdRaUqTV3iB97I-t1kFk5SqjwhJx0wi50a-oOhBbtG1d4sxT4m0EdT7uaKImTsVoFI_tsYJPoxIAHSVg