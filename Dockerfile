FROM python:3.6-alpine

RUN apk update && \
    apk add g++ gcc libxml2 libxslt-dev && \
    apk add --no-cache jpeg-dev zlib-dev && \
    apk add --no-cache --virtual .build-deps build-base linux-headers

RUN adduser -D flasky
USER flasky

WORKDIR /home/flasky

COPY ./requirements.txt requirements.txt

RUN python -m venv venv
RUN source venv/bin/activate
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt

COPY . .

EXPOSE 5000

ENTRYPOINT [ "venv/bin/python", "app.py" ]