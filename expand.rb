#!/usr/bin/env ruby

require 'json'

dict = JSON.parse(File.read('gl-license-scanning-report.json'))

dict.dig('licenses').each do |l|
  if l['name'].size < 5 
    l['name'] = "#{l['name']} License"
  end
end

File.write("expanded.json", JSON.pretty_generate(dict))

